#!/bin/bash
DOCKER_DIR=`dirname ${0}`
source ${DOCKER_DIR}/settings.sh ${DOCKER_DIR}

echo "docker rmi ${IMAGE_NAME}"
docker rmi ${IMAGE_NAME}
