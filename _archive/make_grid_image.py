#!/usr/bin/env python
'''
'''
import cv2
import numpy as np


# 画像サイズ
height, width = 1080, 1920
# 格子線の色
color = (255, 255, 255)
# 格子線の太さ
thickness = 2
# 格子の間隔
grid_size = 50


def main():
    # 黒画像を生成する。
    image = np.zeros((height, width, 3), dtype=np.uint8)

    # 画像の中心を計算する
    center_x, center_y = width // 2, height // 2

    # 垂直線を中心から描画する。
    for offset in range(0, center_x, grid_size):
        start_point_left = (center_x - offset, 0)
        end_point_left = (center_x - offset, height)
        image = cv2.line(image, start_point_left, end_point_left, color, thickness)

        if offset != 0:  # 中心線はすでに描画されているのでスキップ
            start_point_right = (center_x + offset, 0)
            end_point_right = (center_x + offset, height)
            image = cv2.line(image, start_point_right, end_point_right, color, thickness)

    # 水平線を中心から描画する。
    for offset in range(0, center_y, grid_size):
        start_point_up = (0, center_y - offset)
        end_point_up = (width, center_y - offset)
        image = cv2.line(image, start_point_up, end_point_up, color, thickness)

        if offset != 0:  # 中心線はすでに描画されているのでスキップ
            start_point_down = (0, center_y + offset)
            end_point_down = (width, center_y + offset)
            image = cv2.line(image, start_point_down, end_point_down, color, thickness)

    cv2.imwrite('grid.png', image)


if __name__ == '__main__':
    main()
