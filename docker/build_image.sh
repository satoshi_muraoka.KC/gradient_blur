#!/bin/bash
DOCKER_DIR=`dirname ${0}`
source ${DOCKER_DIR}/settings.sh ${DOCKER_DIR}

echo "docker build ${BUILD_OPTION}"
start_time=`date "+%Y-%m-%d %H:%M:%S"`
docker build ${BUILD_OPTION}
end_time=`date "+%Y-%m-%d %H:%M:%S"`
echo ${start_time}
echo ${end_time}
