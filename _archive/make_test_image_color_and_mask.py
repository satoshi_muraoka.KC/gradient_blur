import cv2
import numpy as np

def main():
    bk_img = cv2.imread("grid.png").astype(np.uint8)
    obj_img = cv2.imread("object.png").astype(np.uint8)

    mask = np.where((obj_img == [0, 0, 0]).all(axis=2), 255, 0).astype('uint8')
    cv2.imwrite("test_mask.png", cv2.bitwise_not(mask))

    # Invert the mask (optional step depending on your image)
    inverse_mask = cv2.bitwise_not(mask)

    # Apply the masks
    masked_bk_img = cv2.bitwise_and(bk_img, bk_img, mask=mask)
    masked_test_img = cv2.bitwise_and(obj_img, obj_img, mask=inverse_mask)

    # Combine the two images
    result = cv2.add(masked_bk_img, masked_test_img)

    # Save the result
    cv2.imwrite("test_color.png", result)

if __name__ == "__main__":
    main()
