#!/usr/bin/env python
import argparse
import copy
import cv2
import numpy as np

from itertools import count


class ImageWithMask(object):
    """A image&mask format for random pasting."""

    def __init__(self, img, mask):
        self.img = img
        self.mask = mask
        self.bg_img = copy.deepcopy(img)
        self.bg_mask = np.zeros(self.bg_img.shape[:2], dtype=np.uint8)

    def smoothing_only_edge(self, type="blur"):
        '''オブジェクトのエッジ部分にぼかし処理を加える。
        マスクからオブジェクトのエッジを抽出する。
        抽出したエッジを大きく拡張してぼかし処理のマスクとし、このマスク部分に弱いぼかしを加える。
        前段よりも少し小さく拡張したエッジをぼかし処理のマスクとして、このマスク部分に前段よりも強いぼかしを加える。
        以上をエッジが拡張できなくなるまで繰り返すことで、オブジェクトのエッジ部分でぼかしが最も強く、離れるにつれてぼかしが弱くなる画像を生成する。
        '''
        # オブジェクトごとに処理する。
        non_zero_elements = np.unique(self.mask)[np.unique(self.mask) != 0]
        for mask_id in non_zero_elements:
            # オブジェクトごとにマスクを取り出す。
            individual_mask = np.where(self.mask == mask_id, 255, 0).astype(np.uint8)
            # マスク画像の上下左右に1pixelパディングする。画面端にあるオブジェクトのエッジを適切に取得するため。
            padded_individual_mask = np.zeros([individual_mask.shape[0] + 2, individual_mask.shape[1] + 2]).astype(np.uint8)
            padded_individual_mask[1:1+individual_mask.shape[0],1:1+individual_mask.shape[1]] = individual_mask
            padded_edges = cv2.Canny(padded_individual_mask, 253, 254)

            # オブジェクトの最小外接矩形を探索し、辺の長さを取得する。
            contours, _ = cv2.findContours(individual_mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
            contour = contours[0]
            center, size, angle = cv2.minAreaRect(contour)
            side1, side2 = size

            # 膨張処理の最大イテレーションを計算する（短辺の10%）。
            calc_iteration_rate = 0.1
            max_iterations = int(side1 * calc_iteration_rate) if side1 < side2 else int(side2 * calc_iteration_rate)

            # 最大イテレーションのときカーネルサイズを最小（最も弱いぼかし）にし、イテレーションが順に小さくなるにつれてカーネルサイズを順に大きく（強いぼかし）する。
            # 例：最大イテレーションが7のとき生成されるイテレータは [イテレーション, カーネルサイズ] = [[7, 3], [5, 5], [3, 7], [1, 9]]
            for iterations, kernel_size in zip(range(max_iterations, 0, -2), count(3, 2)):
                # print(iterations, kernel_size)
                filter_size = (kernel_size, kernel_size)
                # エッジ部分を拡張する。
                padded_dilated_edges = cv2.dilate(padded_edges, None, iterations=iterations)

                # ぼかしフィルターを適用する。
                if type=="blur":
                    # 平均化フィルターを適用
                    blurred_img = cv2.blur(self.img, filter_size)
                elif type=="GaussianBlur":
                    # ガウシアンフィルターを適用
                    blurred_img = cv2.GaussianBlur(self.img, filter_size, 0)
                elif type=="medianBlur":
                    # メディアンフィルターを適用
                    blurred_img = cv2.medianBlur(self.img, kernel_size)
                elif type=="bilateral":
                    # バイラテラルフィルターを適用
                    blurred_img = cv2.bilateralFilter(self.img, 9, 75, 75)

                # 元の画像とぼかした画像をエッジ情報に基づいて組み合わせる。エッジ画像の上下左右のパディングを除去したうえで突き合せる。
                self.img = np.where(padded_dilated_edges[1:-1,1:-1,None]==255, blurred_img, self.img)


def parse_args():
    parser = argparse.ArgumentParser(
        description='Smoothing only edge')

    return parser.parse_args()


def main():
    args = parse_args()

    # カラー画像を読み込む。
    color_img = cv2.imread("src_images/test_color.png")
    # マスク画像を読み込む。
    mask_img = cv2.imread("src_images/test_mask.png", 0)

    mask_img = mask_img.astype(np.uint8)
    # オブジェクトのエッジ部分をぼかす。
    image_with_mask = ImageWithMask(img=color_img, mask=mask_img)
    image_with_mask.smoothing_only_edge()

    # カラー画像を保存する。
    cv2.imwrite("result.png", image_with_mask.img)


if __name__ == '__main__':
    main()
