#!/bin/bash
DOCKER_DIR=`dirname ${0}`
source ${DOCKER_DIR}/settings.sh ${DOCKER_DIR}

echo "docker stop ${CONTAINER_NAME}"
docker stop ${CONTAINER_NAME}
